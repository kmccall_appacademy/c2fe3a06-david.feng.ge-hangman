class Hangman
  attr_reader :guesser, :referee, :board
end

class HumanPlayer
end

class ComputerPlayer

  def initiate(filename)
    @words = File.read(filename).split("\n")
  end

  def pick_secret_word
    @words.sample.chomp
  end

  def check_guess

  end


end
